package android.ojedas.todo;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.LayoutRes;
import android.app.Fragment;
import android.os.Bundle;

import java.util.ArrayList;


public class MainActivity extends SingleFragmentActivity  implements  ActivityCallback{
        public ArrayList<TodoPost> todoPost = new ArrayList<TodoPost>();

        @LayoutRes
        @Override
        protected int getLayoutResId() {
            return R.layout.activity_fragment;
        }

        @Override
        protected Fragment createFragment() {
            return new TodoListFragment();
        }

        @Override
        public void onPostSelected(Uri redditpostUri) {


        }

//    @Override
//    public void onPostSelected(int pos) {
//        currentItem = pos;
//        Fragment newFragment = new ItemFragment();
//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
//        transaction.replace(R.id.fragment_container, newFragment);
//        transaction.addToBackStack(null);
//        transaction.commit();
//    }

}
