package android.ojedas.todo;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TodoListFragment extends Fragment{
    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private MainActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
        this.activity = (MainActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_todo_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        TodoPost i1 = new TodoPost("hi", "pj", "ehy", "swee");
        TodoPost i2 = new TodoPost("hi2", "pj4", "ehy7", "swee0");
        TodoPost i3 = new TodoPost("hi8", "pj5", "ehy1", "swee3");
        TodoPost i4 = new TodoPost("hi6", "pj9", "ehy10", "swee11");

        activity.todoPost.add(i1);
        activity.todoPost.add(i2);
        activity.todoPost.add(i3);
        activity.todoPost.add(i4);

        ToDoAdapter adapter = new ToDoAdapter(activityCallback, activity.todoPost);
        recyclerView.setAdapter(adapter);

        return view;
    }

}
