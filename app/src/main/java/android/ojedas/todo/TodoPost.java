package android.ojedas.todo;


public class TodoPost {
    public String title;
    public String dateAdded;
    public String dateDue;
    public String Category;

    public TodoPost(String title, String dateAdded, String dateDue, String Category){
        this.title = title;
        this.dateAdded = dateAdded;
        this.dateDue = dateDue;
        this.Category = Category;

    }
}
