package android.ojedas.todo;


import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ToDoAdapter extends RecyclerView.Adapter<TodoPostHolder>{

    private ArrayList<TodoPost> TodoPosts;
    private ActivityCallback activityCallback;

    public ToDoAdapter(ActivityCallback activityCallback, ArrayList<TodoPost> TodoPosts){
        this.activityCallback = activityCallback;
        this.TodoPosts = TodoPosts;

    }

    @Override
    public TodoPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new TodoPostHolder(view);
    }

    @Override
    public void onBindViewHolder(TodoPostHolder holder, final int position) {
        holder.titleText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //activityCallback.onPostSelected(redditUri);
            }
        });

        holder.titleText.setText(TodoPosts.get(position).title);

    }

    @Override
    public int getItemCount() {

        return TodoPosts.size();
    }
}
